#pragma once
#include "stdafx.h"
#include "iostream"
#include <vector>
#include <windows.h>

using namespace std;
#include <map>

struct Node
{
	//Constructor.
	explicit Node( int id )
	{
		m_id = id;
	}

	//Add's a dependent node.
	void addDependent( weak_ptr<Node> node )
	{
		m_dependents.push_back(node.lock());
	}

	//Node ID.
	int m_id;
	mutable bool m_visited;


	//Vector of dependent nodes.
	vector< shared_ptr<Node> > m_dependents;

};

struct Graph
{

	//Initialize all nodes and set dependents.
	void initialize()
	{
		shared_ptr<Node> node0(new Node(0));
		addNode( node0 );
		shared_ptr<Node> node1(new Node(1));
		addNode( node1 );
		shared_ptr<Node> node2(new Node(2));
		addNode( node2 );
		shared_ptr<Node> node3(new Node(3));
		addNode( node3 );
		shared_ptr<Node> node4(new Node(4));
		addNode( node4 );
		shared_ptr<Node> node5(new Node(5));
		addNode( node5 );
		shared_ptr<Node> node6(new Node(6));
		addNode( node6 );
		shared_ptr<Node> node7(new Node(7));
		addNode( node7 );
		shared_ptr<Node> node8(new Node(8));
		addNode( node8 );
		shared_ptr<Node> node9(new Node(9));
		addNode( node9 );

		node1->addDependent(node0);
		node2->addDependent(node0);
		node3->addDependent(node1);
		node3->addDependent(node2);
		node4->addDependent(node3);
		node5->addDependent(node4);
		node5->addDependent(node8);
		node6->addDependent(node3);
		node6->addDependent(node4);
		node7->addDependent(node3);
		node7->addDependent(node4);
		node8->addDependent(node6);
		node8->addDependent(node7);
		node9->addDependent(node1);
		node9->addDependent(node8);

        for ( int i = m_nodes.size(); i < 100000; ++i )
        {
            shared_ptr<Node> node( new Node( i ) );
			node->m_id = 1000 + i;
            addNode( node );
            node9->addDependent( node );
        }

	}
	//Adds a new node to the beginning of m_nodes vector.
	void addNode( shared_ptr<Node> node )
	{
		node->m_visited = false;
		m_nodes.push_back(node);
		m_nodesMap[node->m_id] = node;
	}

	//Returns Node* with m_id matching the argument id.
	shared_ptr<Node> getNode( int id )
	{
		for (unsigned int i = 0; i < m_nodes.size(); i++)
		{
			if (m_nodes[i]->m_id == id)
				return m_nodes[i];
		}
		return NULL;
	}

	//Vector of shared_ptr<Node> of all nodes in the graph.
	vector< shared_ptr<Node> > m_nodes;
	map< int, shared_ptr< Node> > m_nodesMap;

};


class DAG
{
	//Typpedef Function Pointer.
	typedef void (DAG::*VisitFn)(const Node& node );

	//If the ID is not already in g_printedIDs, it's printed to console and added to the vector.
	void printID(const Node& node);

	void resetCallCount();
	void countCalls( const Node& node );
	void printCallCounts();

	//Traverses through graph and calls the visit function for all nodes. 
	void traverse( const Node& root, VisitFn visit );
    void traverseRecursive( const Node& root, VisitFn visit );
	void traverseIterative( const Node& root, VisitFn visit );
	
	//Sets all of the nodes m_visited variable in the recursion to false.
	void traverseRecursiveReset( const Node& root );

	//The graph of nodes
	shared_ptr<Graph> m_graph;

    int m_traverseCount;

public : 
	//Initializes DAG
	void initialize();

	//Returns a shared pointer to m_graph
	shared_ptr<Graph> getGraph();

	typedef std::map<int, int> MapType;
	//Map of ids to call-count
	//key is id, value is call count
	MapType m_callCount;

};

