// Programming Test.cpp : main project file.

#include "stdafx.h"
#include "DAG.h"
#include "iostream"
#include <vector>
#include <windows.h>

using namespace std;

//Debug Code (Memory leak detection)
//Using CRT lib
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW   
#endif
#endif

//Main
void main()
{
	//Debug code (Memory leak detection)
#ifdef _DEBUG
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	//Memory allocation breakpoint
	//_crtBreakAlloc = 191;
#endif

	//Setting up input
	HANDLE handle;
	DWORD  mode;
	handle = GetStdHandle( STD_INPUT_HANDLE );
	GetConsoleMode( handle, &mode );
	SetConsoleMode( handle, ENABLE_ECHO_INPUT | ENABLE_PROCESSED_INPUT );

	//Create and initialize directed acyclic graph
	shared_ptr<DAG> dag(new DAG());
	//initialize contains program loop
	dag->initialize();

	SetConsoleMode( handle, mode );
	return;
}


