#ifndef IMR_WIN32_HRTIMER_H
#define IMR_WIN32_HRTIMER_H

#include <Windows.h>

class HRTimer
{
public:
    typedef ULONGLONG Tick;
    static Tick currentTick()
    {
        LARGE_INTEGER result;
        result.QuadPart = 0;
        ::QueryPerformanceCounter( &result );
        return static_cast<ULONGLONG>( result.LowPart );
    }
public:
    HRTimer()
    {
        LARGE_INTEGER freq;
        QueryPerformanceFrequency( &freq );
        m_conversion = 1.0f / freq.QuadPart;
        restart();
    }

    void restart()
    {
        m_running = true;
        ::QueryPerformanceCounter( &m_start );
    }

    void stop()
    {
        ::QueryPerformanceCounter( &m_stop );
        m_running = false;
    }

    float lap()
    {
        LARGE_INTEGER end;
        if ( m_running )
        {
            ::QueryPerformanceCounter( &end );
        }
        else
        {
            end = m_stop;
        }

        float result = (end.QuadPart - m_start.QuadPart) * m_conversion;
        m_start = end;

        return result;
    }

    float elapsed()
    {
        LARGE_INTEGER end;
        if ( m_running )
        {
            ::QueryPerformanceCounter( &end );
        }
        else
        {
            end = m_stop;
        }

        return (end.QuadPart - m_start.QuadPart) * m_conversion;

    }
private:
    LARGE_INTEGER m_start;
    LARGE_INTEGER m_stop;
    float m_conversion;
    bool m_running;
};

#endif

