#include "stdafx.h"
#include "DAG.h"
#include "hrtimer.h"
#include <ctime>
#include <stack>


double diffclock(clock_t clock1,clock_t clock2)
{
    double diffticks=clock2-clock1;
    double diffms=(diffticks)/(CLOCKS_PER_SEC/1000);
    return diffms;
}

void DAG::traverse( const Node& root, DAG::VisitFn visit )
{
    m_traverseCount = 0;
	//traverseRecursive( root, visit );
	//traverseRecursiveReset( root );
	traverseIterative( root, visit );

}

void DAG::traverseRecursive( const Node& root, VisitFn visit )
{
    ++m_traverseCount;
	if (&root != NULL)
	{
		for(unsigned int i = 0; i < root.m_dependents.size(); i++)
		{
			if( !root.m_dependents[i]->m_visited )
			{
				root.m_dependents[i]->m_visited = true;
				traverseRecursive(*root.m_dependents[i], visit);
			}
		}
		//Call the visit function
		(*this.*visit)(root);
	}
}

void DAG::traverseRecursiveReset( const Node& root )
{
	if (&root != NULL)
	{
		for(unsigned int i = 0; i < root.m_dependents.size(); i++)
		{
			if( root.m_dependents[i]->m_visited )
			{
				root.m_dependents[i]->m_visited = false;
				traverseRecursiveReset(*root.m_dependents[i]);
			}
		}
	}
}

void DAG::traverseIterative( const Node& root, VisitFn visit )
{
	if (&root != NULL)
	{
		stack<int> stk;
		stk.push(root.m_id);
		while(!stk.empty())
		{
			++m_traverseCount;
			shared_ptr<Node> temp = m_graph->m_nodesMap[stk.top()];
			stk.pop();
			for(unsigned int i = 0; i < temp->m_dependents.size(); i++)
			{
				if( !temp->m_dependents[i]->m_visited )
				{
					temp->m_dependents[i]->m_visited = true;
					stk.push(temp->m_dependents[i]->m_id);
				}
			}
		}
		stk.push(root.m_id);
		while(!stk.empty())
		{
			shared_ptr<Node> temp = m_graph->m_nodesMap[stk.top()];
			stk.pop();
			for(unsigned int i = 0; i < temp->m_dependents.size(); i++)
			{
				if( temp->m_dependents[i]->m_visited )
				{
					temp->m_dependents[i]->m_visited = false;
					stk.push(temp->m_dependents[i]->m_id);
				}
			}
		}
	}
}

void DAG::resetCallCount()
{
    //reset to zero rather than clearing, so we don't get the
    //cost of reallocating map elements on subsequent calls to countCalls
    for ( auto i = m_callCount.begin(), end = m_callCount.end(); i != end; ++i )
    {
        i->second = 0;
    }
}

void DAG::countCalls( const Node& node )
{
    //see if node already exists in the map
    MapType::iterator i = m_callCount.lower_bound( node.m_id );
    if ( i != m_callCount.end() && !(m_callCount.key_comp()( node.m_id, i->first ) ) )
    {
        //if so increment the call count
        ++i->second;
    }
    else
    {
        //else insert id and set the initial call count to 1
        m_callCount.insert( i, MapType::value_type( node.m_id, 1 ) );
    }
}

void DAG::printCallCounts()
{
    for ( MapType::iterator i = m_callCount.begin(), end = m_callCount.end(); i != end; ++i )
    {
        if ( i->second )
        {
            std::printf( "Node %d visit called %d time(s)\n", i->first, i->second );
        }
    }
}

void DAG::printID(const Node& node) 
{ 
	printf("%d\n", node.m_id); 
	return;
}

shared_ptr<Graph> DAG::getGraph()
{
	return m_graph;
}

void DAG::initialize()
{
	m_graph = make_shared<Graph>();
	m_graph->initialize();

	bool cont = true;
	while (cont)
	{
		cout << "Which node would you like to travers from? (press any letter to exit)\n" << flush;
		int input = cin.get();
		int startNode = 0;
		switch (input)
		{
		case 48: 
			startNode = 0;
			break;
		case 49: 
			startNode = 1;
			break;
		case 50: 
			startNode = 2;
			break;
		case 51: 
			startNode = 3;
			break;
		case 52: 
			startNode = 4;
			break;
		case 53: 
			startNode = 5;
			break;
		case 54: 
			startNode = 6;
			break;
		case 55: 
			startNode = 7;
			break;
		case 56: 
			startNode = 8;
			break;
		case 57: 
			startNode = 9;
			break;
		default:
			cont = false;
			break;
		}
		if (cont)
		{
			Node& node = *(getGraph()->getNode(startNode));
			//getGraph()->setVisitedValuesToFalse();

			//DAG::VisitFn func = &DAG::printID;
			//traverse(node, func );

			//optimization clock, start
			//clock_t start = clock();
            resetCallCount();

            HRTimer timer;
            traverse( node, &DAG::countCalls );
            float elapsed = timer.elapsed();
            //printCallCounts();
            std::printf( "Total traverse count: %d\n", m_traverseCount );
			std::printf( "Total time in seconds: %.6f\n", elapsed );

			//optimization clock, stop and print
			//clock_t end = clock();
			//std::printf( "Total time(ms): %f\n", diffclock(start, end) );

		}
	};
}
